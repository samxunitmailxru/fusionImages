# fusionImages
Tous les codes nécessaires pour cacher une image dans une autre image et la récupérer. (Lien vidéo youtube : https://youtu.be/QP9ZMG_XatA)


fonction.py << contient toutes les fonctions utiles

processus.py << exemples d'utilisation

soft.py << programme graphique (nécessite d'installer des librairies complémentaires)


+ 3 images en png (seul format pris en charge).

Attention selon les combinaisons le résultat peut ne pas être satisfaisant. 
Par exemple avec cartes.png qui est presque une image en noir et blanc il est compliqué d'y cacher des images très colorées.
